﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using EzEditor;
using System.Collections;

[CustomEditor(typeof (GameData))]
public class GameDataInspector : Editor
{
	private GameData _target;
	private bool _showClassChangers;
	private KeyValuePair<string, GameObject> _newClassChanger;

	public override void OnInspectorGUI()
	{
		if (_target == null)
			_target = target as GameData;

		DrawDefaultInspector();

		using (gui.Horizontal()) {
			GameData.RoundTime = gui.EzFloatField("Round Time", GameData.RoundTime, 20f, GUILayout.Width(100f));
			GameData.MaxNumberOfPlayersPerTeam = gui.EzIntField("Player/Team", GameData.MaxNumberOfPlayersPerTeam, 20f);
		}
		if (gui.EzButton("Draw")) {
			//_lineMesh.DrawLine();
		}
		_target.ClassChangers = gui.EzDict("Class Changers", _target.ClassChangers, ref _showClassChangers, ref _newClassChanger);
		

		if (GUI.changed)
			EditorUtility.SetDirty(target);
	}
	
}
